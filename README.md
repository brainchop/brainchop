# brainchop

## develop

- install the latest [nodejs](http://nodejs.org) (8.x+)
- `sudo npm install -g yarn`
- `yarn && yarn run bootstrap`
  - this will install all dependencies, including sub-package deps.
- `yarn run build` - builds all sub-packages/services

## run

to run in dev mode, run `yarn start` in each service (see the `packages/` directory).  defaults are all configured to work on `localhost`.

running/deploying production builds is not covered, and is left as an exercise to the reader.

