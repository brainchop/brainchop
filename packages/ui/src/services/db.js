import * as Pouchy from 'pouchy'

export function create ({ host, port, name }) {
  if (!host || !port || !name) {
    throw new Error('host, port, and db name (store name) are required')
  }
  return new Pouchy({
    url: `http://${host}:${port}/${name}`
  })
}
