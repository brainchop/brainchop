import React, { Component } from 'react'
import Dropzone from 'react-dropzone'

export default class FileDropzone extends Component {
  constructor (props) {
    super(props)
    this.state = {
      accepted: [],
      rejected: []
    }
  }

  render () {
    let rejected
    if (this.state.rejected.length) {
      rejected = (
        <div>
          <h4>Rejected files</h4>
          <ul>
            {
              this.state.rejected.map(f => <li key={f.name}>{f.name} - {f.size} bytes</li>)
            }
          </ul>
        </div>
      )
    }
    return (
      <section>
        <div className='dropzone'>
          <Dropzone
            onDrop={(accepted, rejected) => {
              this.setState({ accepted, rejected })
              if (this.props.onDrop) this.props.onDrop({ accepted, rejected })
            }}
          >
            <p>Try dropping some files here, or click to select files to upload.</p>
          </Dropzone>
        </div>
        <aside>
          <h4>Accepted files</h4>
          <ul>
            {
              this.state.accepted.map(f => <li key={f.name}>{f.name} - {f.size} bytes</li>)
            }
          </ul>
          {rejected}
        </aside>
      </section>
    )
  }
}
