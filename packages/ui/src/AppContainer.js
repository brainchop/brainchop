import React, { Component } from 'react'
import axios from 'axios'
import App from './App'

class AppContainer extends Component {
  constructor (props) {
    super(props)
    this.db = null
    this.state = {
      things: []
    }
    this.addThing = this.addThing.bind(this)
    this.uploadFile = this.uploadFile.bind(this)
    this.onFileDrop = this.onFileDrop.bind(this)
  }
  patchState (patch) {
    this.setState(Object.assign({}, this.state, patch))
  }
  async componentWillMount () {
    const host = process.env.REACT_APP_SERVER_HOST
    const port = process.env.REACT_APP_SERVER_PORT
    this.origin = `http://${host}:${port}`
    // this.db = createDb({
    //   host: process.env.REACT_APP_SERVER_HOST,
    //   port: process.env.REACT_APP_SERVER_PORT,
    //   name: 'things'
    // })
    setInterval(async () => {
      // const things = await this.db.all()
      const { data: things } = await axios.get(`${this.origin}/things`)
      this.patchState({ things })
    }, 1000) // use websockets, not dirty polling
  }
  addThing () {
    axios.post(`${this.origin}/things`, {
      random: Math.random()
    })
  }
  onFileDrop ({ accepted, rejected }) {
    this.queueForUpload(accepted)
  }
  queueForUpload (files) {
    // no queuing yet...
    files.forEach(file => this.uploadFile(file))
  }
  async uploadFile (file) {
    var data = new window.FormData()
    data.append('file', file)
    data.append('name', file.name || 'FNAME')
    var config = {
      headers: {
        'Content-Type': 'multipart/form-data'
      },
      onUploadProgress: function (progressEvent) {
        var percentCompleted = Math.round((progressEvent.loaded * 100) / progressEvent.total)
        console.log(`[upload] ${file.name} ${percentCompleted}%`)
      }
    }
    await axios.put(`${this.origin}/uploads`, data, config)
  }
  render () {
    return <App
      {...this.state}
      {...{
        onFileDrop: this.onFileDrop,
        addThing: this.addThing
      }}
    />
  }
}

export default AppContainer
