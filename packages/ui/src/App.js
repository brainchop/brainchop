import React from 'react'
import logo from './logo.svg'
import './App.css'
import { Button, ButtonToolbar, Col, Grid, Row } from 'react-bootstrap'
import FileDropzone from './FileDropzone'

function App (props) {
  const { things } = props
  return (
    <div className='App'>
      <header className='App-header'>
        <img src={logo} className='App-logo' alt='logo' />
        <h1 className='App-title'>brainchop</h1>
      </header>
      <Grid className='App-body'>
        <Row>
          <Col xs={6} xsOffset={3}>
            <ul>
              {things.map((thing, i) => <li key={i}>{JSON.stringify(thing)}</li>)}
            </ul>
            <FileDropzone onDrop={props.onFileDrop} />
            <ButtonToolbar>
              <Button bsStyle='warning' onClick={props.addThing}>Add a random thing</Button>
              <Button bsStyle='primary'>Upload file</Button>
            </ButtonToolbar>
          </Col>
        </Row>
      </Grid>
      <footer>
        Brain icon from the Noun Project
      </footer>
    </div>
  )
}

export default App
