# db

this is the brainchop database.


## todo

if we need migrations fo the DB, this db is good to go.
however, this is a frivolous image build ATM.  in the future, just mount a couchdb db.

## usage

- build
  - start docker
  - `npm run build`

- run
  - `npm start`, or, `docker run -it -p 5984:5984 brainchop_db`
  - follow the onscreen instructions for entering into the db admin area

