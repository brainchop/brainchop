const debug = require('debug')('queue')

const STOPPED = 0
const RUNNING = 1

class Queue {
  constructor (opts) {
    const { concurrency } = opts || {}
    this.queue = []
    this.state = STOPPED
    this.concurrency = concurrency || 3
    this.wip = 0
  }
  add (fn) {
    this.queue.push(fn)
    debug(`[queue ${this.queue.length} queued job`)
    this.run()
  }
  onComplete (res) {
    --this.wip
    debug(`[wip ${this.wip}] wip job finished`)
    this.run()
  }
  run () {
    if (this.state === RUNNING) return
    while (this.queue.length && this.wip < this.concurrency) {
      ++this.wip
      debug(`[wip ${this.wip}] wip job started`)
      let fn = this.queue.shift()
      debug(`[queue ${this.queue.length}] dequeued job to wip`)
      Promise.resolve(fn()).then(this.onComplete.bind(this))
    }
  }
}

module.exports = Queue

module.exports = Queue
