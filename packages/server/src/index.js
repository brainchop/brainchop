'use strict'

var Koa = require('koa')
var bodyParser = require('koa-better-body')
var { get, post, put } = require('koa-route')
var simpleResponses = require('./simple-responses')
var { create: createDb } = require('./db')
var cors = require('koa-cors')
const path = require('path')
const fs = require('fs-extra')
const execa = require('execa')
const Queue = require('./queue')
process.env.NODE_ENV = process.env.NODE_ENV || 'production'

var isDev = !!process.env.NODE_ENV.match(/dev/)

const UPLOADS_DIR = path.join(__dirname, '..', 'uploads')
var THING_NAME = 'thing'

module.exports = {
  async start ({
    port,
    dbPort,
    dbHost
  }) {
    await fs.mkdirp(UPLOADS_DIR)
    var db = createDb({ port: dbPort, host: dbHost, name: THING_NAME })
    var app = new Koa()
    var queue = new Queue({ concurrency: 3 })
    if (isDev) app.use(cors())
    app.use(bodyParser())
    app.use(simpleResponses)
    app.use(get(`/`, (ctx) => ({ hello: ', world' })))
    app.use(get(`/${THING_NAME}s`, (ctx) => db.all()))
    app.use(get(`/${THING_NAME}s/:id`, (ctx, id) => db.get(id)))
    app.use(post(`/${THING_NAME}s`, (ctx, doc) => db.save(ctx.request.fields)))
    app.use(put(`/uploads`, async (ctx, doc) => {
      // security risk - similarly named files can squash each other
      // maybe just put the file metadata to the db and hash the filename?
      await Promise.all(
        ctx.request.files.map(file => fs.move(file.path, path.join(UPLOADS_DIR, file.name)))
      )
      const newFilenames = ctx.request.files.map(file => path.join(UPLOADS_DIR, file.name))
      newFilenames.map(filename => {
        return queue.add(() => {
          // TODO do something interesting here... like run neural net!
          console.log('listing information of newly uploaded file: ' + filename)
          return execa('ls', ['-al', filename], { stdio: 'inherit' })
        })
      })
    }))
    this.server = app.listen(port)
    console.log(`starting brainchop on port ${port}`)
  },
  async stop () {
    console.log('stopping brainchop')
    this.server.close()
  }
}
