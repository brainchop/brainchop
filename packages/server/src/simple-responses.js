module.exports = async function simpleResponses (ctx, next) {
  try {
    const res = await next()
    ctx.body = res
  } catch (err) {
    console.error(err)
    ctx.body = { error: err.message }
    ctx.status = err.status || 500
  }
}
