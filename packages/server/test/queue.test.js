'use strict'

process.env.DEBUG = '*'
var test = require('ava')
var Queue = require('../src/queue')
var bb = require('bluebird')
var times = require('lodash/times')

test('queue', async function (t) {
  var concurrency = 3
  var q1 = new Queue({ concurrency })
  var numJobs = 10
  var ticks = 0
  var maxConcurrencyObserved = 0
  var tick = async () => {
    await bb.delay(50)
    ++ticks
  }
  times(numJobs, i => q1.add(tick))
  while (ticks !== 10) { // eslint-disable-line
    if (q1.wip > maxConcurrencyObserved) maxConcurrencyObserved = q1.wip
    t.truthy(q1.wip <= concurrency)
    await bb.delay(2)
  }
})
