'use strict'

require('perish')
const brainchopServer = require('../')

brainchopServer.start({
  port: process.env.PORT || 8000,
  dbPort: process.env.DB_PORT || 5984,
  dbHost: process.env.DB_HOST || 'localhost'
})
